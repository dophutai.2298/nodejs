//require = import ... from...
const express = require('express');
const morgan = require('morgan');
const handlebars = require('express-handlebars');
const path = require('path');
const route = require('./routers');
const db = require('./config/db');

//Connect to DB
db.connect();

const app = express();
const port = 3000;

//Kiểm tra path để trả về file tĩnh:
app.use(express.static(path.join(__dirname, 'public')));

//HTTP logger
app.use(morgan('combined'));

//Middleware, read req.body
app.use(express.urlencoded({
    extended: true
}));
app.use(express.json());

//HTTP logger
//XMLHttpRequest, fetch, axios

//Template engine
app.engine('hbs', handlebars({
    //Đổi đuôi tên cho gọn=> hbs=handlebars
    extname: '.hbs'
}));
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'resources/views'));
console.log('PATH:', path.join(__dirname, 'resources/views'))



//Routes init
route(app);

/*---------Đã chuyển về file route------- */
//render: render 1 handlebars
//app.get('/', (req, res) => res.render('home'));

//app.get('/news', (req, res) => res.render('news'))


//Action ---> Dispatcher ---> Function handler
//app.get('/search', (req, res) => { res.render('search'); });

//send: gửi về 1 ... formData: req.body
//app.post('/search', (req, res) => { console.log(req.body); res.send(''); });


app.listen(port, () => console.log(`App listerning at http://localhost:${port}`))