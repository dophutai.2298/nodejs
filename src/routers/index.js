const newRouter = require('./news');
const siteRouter = require('./site');

function route(app) {

    app.use('/news', newRouter);

    //render: render 1 handlebars
    //app.get('/', (req, res) => res.render('home'));
    app.use('/', siteRouter);



    //Action ---> Dispatcher ---> Function handler
    //app.get('/search', (req, res) => { res.render('search'); });
    // app.use('/', homeRouter);
    //send: gửi về 1 ... formData: req.body
    //app.post('/search', (req, res) => { console.log(req.body); res.send(''); });
}

module.exports = route;